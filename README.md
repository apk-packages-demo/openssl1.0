For man pages see [alpinelinux-packages-demo/openssl1.0-doc](https://gitlab.com/alpinelinux-packages-demo/openssl1.0-doc)@gitlab
* [openssl req batch](https://www.google.com/search?q=openssl+req+batch)
  * [*HowTo: Create CSR using OpenSSL Without Prompt (Non-Interactive)*](https://www.shellhacks.com/create-csr-openssl-without-prompt-non-interactive/)

## Related projects
* [travis-util/Self-Signed-SSL-Certificate](https://github.com/travis-util/Self-Signed-SSL-Certificate)@GitHub